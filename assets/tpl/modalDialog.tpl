{config_load file="$language/lang_$language.conf" section="buttons"}
<div id="modalDialog" class="modal fade" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">{$modalTitle}</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="{#close#}"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                {$modalBody}
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">{#close#}</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<div id="modalDialogFullWidth" class="modal fade" data-keyboard="false" data-backdrop="static" >
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">{$modalTitle}</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="{#close#}"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                {$modalBody}
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">{#close#}</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
