
{config_load file="$language/lang_$language.conf" section="buttons"}

<div id="modal-product-review" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">{#heading_reviews#}</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
            <form  method="post" action="/product_reviews_write.php?action=process&products_id={$product_data.id}">
                <div class="modal-body">
                    <input type="hidden" name="action" value="process" />

                    <div>Ihre Meinung:</div>
                    <textarea rows="5"  id="review" name="review" class="form-control"></textarea>
                    <div><font face="Arial, Helvetica, sans-serif" size="1"><font color="ff0000"><b>ACHTUNG:</b></font> HTML wird nicht unterst&uuml;tzt!</font></div>
                    <div>
                        <br />
                        <strong>Bewertung:&nbsp;<font face="Arial, Helvetica, sans-serif" size="1" color="#FF0000">SCHLECHT</font></strong>
                        <input type="radio" value="1" name="rating">
                        <input type="radio" value="2" name="rating">
                        <input type="radio" value="3" name="rating">
                        <input type="radio" value="4" name="rating">
                        <input type="radio" value="5" name="rating">
                        <strong>SEHR GUT</strong>
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="submit"class="btn btn-success" value="Bewertung abgeben" title="Bewertung abgeben">
                </div>
                {$FORM_END}
        </div>
    </div>
</div>

