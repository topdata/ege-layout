<div class="navbar-bg-color-top">
    <div class="container">
        <nav class="navbar navbar-default navbar-static-top ">
            <div class="nav-item float-right">
                <div class="btn-group btn-group-md navbar-btn">
                    {$box_LANGUAGES}
                    {* {$box_CATEGORIES_FAVORED} *}
                    {$box_SPYLIST}
                    <button type="button" class="btn btn-outline-light link-compare-ajax" title="{#compare#}"><i class="fas fa-exchange-alt"></i> <span class="badge compare_count">{misc->getComparelistCount}</span></button>
                    {if $smarty.session.customer_id}
                        <a href="/wishlist.php" type="button" class="btn btn-outline-light" title="{#wishlist#}" ><i class="far fa-star"></i> <span class="badge wishlist_count">{misc->getWishlistCount}</span></a>
                    {/if}
                    {$box_CART}
                </div>
            </div>
            <div class="toplinks">
                <ul>
                    {$box_LOGIN}
                    {if $box_PRIVAT_COMPANY_PSWITCH}
                        <li>{$box_PRIVAT_COMPANY_PSWITCH}</li>
                    {/if}
                    {$box_ADMIN}
                </ul>
            </div>
        </nav>
    </div>
</div>