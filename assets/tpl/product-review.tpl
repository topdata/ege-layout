
{if $smarty.session.customers_status.customers_status_read_reviews ==1}
    <div>
        <strong>{#heading_reviews#}</strong><br/>
                            <span class="product_rating" itemprop="aggregateRating" itemscope itemtype="http://schema.org/AggregateRating">
                                {$product_data.PRODUCTS_RATING_IMAGE} &#216;
                                <span itemprop="ratingValue">{$product_data.PRODUCTS_RATING}</span>
                                Sterne bei
                                <span itemprop="reviewCount">{$product_data.PRODUCTS_RATING_COUNT}</span>
                                {if $product_data.PRODUCTS_RATING_COUNT == 1}Bewertung{else}Bewertungen{/if}
                             </span>
    </div>
    {if $MODULE_products_reviews}
        {$MODULE_products_reviews}
    {else}
        <div class="no_reviews"><i>{#products_no_reviews#}</i></div>
    {/if}
    {if isset($smarty.session.customer_id)}
        {if $smarty.session.customers_status.customers_status_write_reviews ==1}
            {include file='assets/tpl/product-review-new.tpl'}
            <a href="#modal-product-review" class="link-review" data-toggle="modal" title="{#products_new_review#}">{#products_new_review#}</a>

        {/if}
    {else}
        {#products_no_reviews_login#}
    {/if}
{/if}