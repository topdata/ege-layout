{config_load file="$language/lang_$language.conf" section="index"}
{config_load file="$language/lang_$language.conf" section="product_listing"}
{config_load file="$language/lang_$language.conf" section="product_info"}
{config_load file="$language/lang_$language.conf" section="graduated_price"}
{config_load file="$language/lang_$language.conf" section="buttons"}


<div class="col-12">

    <div class="card product--box p-2" data-page-index="1" data-ordernumber="{$product.PRODUCTS_MODEL}">
        <div class="item-inner">


            <div class="row align-items-center">
                <div class="col-md-5 col-lg-2">
                    <div style="padding-left: 15px;">
                        <a href="{$product.PRODUCTS_LINK}" title="{$product.PRODUCTS_NAME}"
                           class="thumbnail thumbnail-product" style="margin-bottom: 0; border: 0; box-shadow: none;">
                            <img srcset="{$product.PRODUCTS_IMAGE_INFO}" alt="{$product.PRODUCTS_NAME}"
                                 title="{$product.PRODUCTS_NAME}" class="img-fluid">
                        </a>
                    </div>
                </div>

                <div class="col-md-7 col-lg-10">

                    <div class="card-body">
                        <div class="product--info">
                            <div class="row">
                                <div class="col-lg-9">

                                    <a href="{$product.PRODUCTS_LINK}" class="product--title"
                                       title="{$product.PRODUCTS_NAME}">{$product.PRODUCTS_NAME}</a>

                                    <ul class="small">
                                        {if $product.PRODUCTS_MANUFACTURER}
                                            <li>
                                            <b>Hersteller:</b>
                                            {$product.PRODUCTS_MANUFACTURER}</li>{/if}
                                        {if $product.PRODUCTS_MANUFACTURERS_MODEL}
                                            <li>
                                            <b>OEM-Nr.:</b>
                                            {$product.PRODUCTS_MANUFACTURERS_MODEL}</li>{/if}
                                        {if $product.PRODUCTS_EAN}
                                            <li>
                                            <b>EAN:</b>
                                            {$product.PRODUCTS_EAN}</li>{/if}
                                    </ul>
                                    <div class="baseprice-grid">{$module_data.PRODUCTS_BASE_PRICE_FORMATED}</div>
                                </div>


                                <div class="col-lg-3">
                                    <div class="product--price-info">
                                        <div class="price--unit">
                                            {if $product.PRODUCTS_OLD_PRICE > 0}
                                                <span class="old-price">
                                                <span class="price"
                                                      id="old-price-236">{$product.PRODUCTS_OLD_PRICE}</span>
                                            </span>
                                                {if $product.PRODUCTS_SPECIAL_OFFER_PRICE > 0}
                                                    <span class="special-price">
                                                    <span class="price" itemprop="price"
                                                          id="product-price-236">{$product.PRODUCTS_SPECIAL_OFFER_PRICE}</span>
                                                </span>
                                                {else}
                                                    <span class="regular-price">
                                                    <span class="price"
                                                          itemprop="price">{$product.PRODUCTS_PRICE}</span>
                                                </span>
                                                {/if}
                                            {else}
                                                <span class="regular-price">
                                                <span class="product--price"
                                                      itemprop="price">{$product.PRODUCTS_PRICE}</span>
                                            </span>
                                            {/if}
                                        </div>
                                    </div>

                                    <span class="shipping-details"
                                          style="font-size: x-small;">({$module_data.PRODUCTS_TAX_INFO}
                                        ) {if $module_data.PRODUCTS_SHIPPING_LINK}{$module_data.PRODUCTS_SHIPPING_LINK}{/if}</span>


                                    <div class="add-to-links">
                                        <div class="add-to-links-group btn-group">
                                            <button type="button"
                                                    class="btn btn-default bg-button-light-blue link-add-basket"
                                                    data-Qty="1" data-pID="{$product.PRODUCTS_ID}" title="{#basket#}"
                                                    id="add-basket"><i class="fas fa-cart-arrow-down"></i></button>

                                            <a href="{$product.PRODUCTS_LINK}" title="{$product.PRODUCTS_NAME}"
                                               class="btn btn-default bg-button-light-blue" title="{#details#}"><i
                                                        class="fas fa-info"></i></a>
                                            {if $smarty.session.customer_id}
                                                <button type="button"
                                                        class="btn btn-default bg-button-light-blue link-wishlist{if $product.PRODUCTS_ON_WISHLIST == 'true'} active delete{else} add{/if}"
                                                        data-pID="{$product.PRODUCTS_ID}" title="{#wishlist#}"><i
                                                            class="far fa-star"></i></button>
                                            {/if}
                                            <button type="button"
                                                    class="btn btn-default bg-button-light-blue link-compare{if $product.PRODUCTS_COMPARE == 'true'} active delete{else} add{/if}"
                                                    data-pID="{$product.PRODUCTS_ID}" title="{#compare#}"><i
                                                        class="fas fa-exchange-alt"></i></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <br>
</div>