{config_load file="$language/lang_$language.conf" section="index"}
{config_load file="$language/lang_$language.conf" section="product_listing"}
{config_load file="$language/lang_$language.conf" section="product_info"}
{config_load file="$language/lang_$language.conf" section="graduated_price"}
{config_load file="$language/lang_$language.conf" section="buttons"}


<div class="col-md-3 col-sm-4 col-xs-12" style="width: 350px;">
    <!-- BEGIN PRODUCT -->
    <div class="panel panel-default product--box">


        <div class="product--badges">
            {include file="`$assetsTpl`product/colorMark.tpl" colorValue=$product.PRODUCTS_FEATURES.1.features.1.feature_value}
        </div>

        <div class="panel-heading" >

            <a href="{$product.PRODUCTS_LINK}" title="{$product.PRODUCTS_NAME}" class="thumbnail thumbnail-product" style="margin-bottom: 0; border: 0; box-shadow: none;">
                <img srcset="{$product.PRODUCTS_IMAGE_INFO}" alt="{$product.PRODUCTS_NAME}"
                     title="{$product.PRODUCTS_NAME}" class="img-fluid" style="height: 200px;">
            </a>
        </div>

        <div class="panel-body">
            <h4 class="card-title">
                <a href="{$product.PRODUCTS_LINK}" class="product--title"
                   title="{$product.PRODUCTS_NAME}">{$product.PRODUCTS_MANUFACTURERS_MODEL}</a></h4>
            <p class="card-text" style="min-height: 80px;">{$product.PRODUCTS_NAME}</p>
        </div>

        <div class="panel-footer">
            <div class="row">
                <div class="col-md-7 col-sm-7">
                    <div class="product--price-info">
                        <div class="price--unit">
                            {if $product.PRODUCTS_OLD_PRICE > 0}
                                <span class="old-price">
                                <span class="price" id="old-price-236">{$product.PRODUCTS_OLD_PRICE}</span>
                            </span>
                                {if $product.PRODUCTS_SPECIAL_OFFER_PRICE > 0}
                                    <span class="special-price">
                                <span class="price" itemprop="price"
                                      id="product-price-236">{$product.PRODUCTS_SPECIAL_OFFER_PRICE}</span>
                            </span>
                                {else}
                                    <span class="regular-price">
                                <span class="price" itemprop="price">{$product.PRODUCTS_PRICE}</span>
                            </span>
                                {/if}
                            {else}
                                <span class="regular-price">
                            <span class="product--price" itemprop="price">{$product.PRODUCTS_PRICE}</span>
                        </span>
                            {/if}
                            <span class="tax-details" style="font-size: 10px;">({$module_data.PRODUCTS_TAX_INFO})</span>
                        </div>
                    </div>
                </div>
                <div class="col-md-5 col-sm-5">
                    <button class="btn btn-success right link-add-basket" data-Qty="1"
                            data-pID="{$module_data.PRODUCTS_ID}" title="{#basket#}" id="add-basket"><i
                                class="fa fa-cart-plus" aria-hidden="true"></i></button>
                </div>
                <div class="col-md-12 col-sm-12" style="font-size: x-small;">
                    {if $module_data.PRODUCTS_SHIPPING_LINK}{$module_data.PRODUCTS_SHIPPING_LINK}{/if}
                    {if $module_data.PRODUCTS_PRICE_PER_PAGE != ''}
                        <div class="price-per-page">
                        <strong>{#product_price_per_page#} </strong>
                        {$module_data.PRODUCTS_PRICE_PER_PAGE}</div>{/if}
                    <div class="baseprice-grid">{$module_data.PRODUCTS_BASE_PRICE_FORMATED}</div>
                </div>

            </div>
        </div>
    </div>
    <!-- END Product -->
    <br>
</div>