

<li class="item col-md-3 col-sm-4 col-xs-12">
    <div class="item-inner">
        <div class="product-image col-md-12 col-sm-12 col-xs-12">
            <div class="product-image-thumbnail">
                <a href="{$module_data.PRODUCTS_LINK}" title="{$module_data.PRODUCTS_NAME}">
                    <div class="img-thumbnail">
                        <img src="{$module_data.PRODUCTS_IMAGE_INFO}" alt="{$module_data.PRODUCTS_NAME}" />
                    </div>
                </a>
            </div>
            <div class="more-info"></div>

            <div class="reviews-wrap"></div>
        </div>
        {if $module_data.PRODUCTS_SPECIAL_OFFER_PRICE}
            <div class="labels specialoffer"></div>
        {/if}
        <div class="price-review col-md-12 col-sm-12 col-xs-12">
            <h4 class="product-name"><a href="{$module_data.PRODUCTS_LINK}" title="{$module_data.PRODUCTS_NAME}">{$module_data.PRODUCTS_NAME}</a></h4>
            <div class="price-box">

                {if $module_data.PRODUCTS_OLD_PRICE > 0}
                    <p class="old-price">
                        <span class="price-label">Regul�rer Preis:</span>
                        <span class="price" id="old-price-236">{$module_data.PRODUCTS_OLD_PRICE}</span>
                    </p>

                    <p class="special-price">
                        <span class="price-label">Special Price</span>
                        <span class="price" itemprop="price" id="product-price-236">{$module_data.PRODUCTS_SPECIAL_OFFER_PRICE}</span>
                    </p>
                {else}
                    <span class="regular-price" id="product-price-134">
                            <span class="price" itemprop="price">{$module_data.PRODUCTS_PRICE}</span>
                        </span>
                {/if}
            </div>
            <span class="tax-details">({$module_data.PRODUCTS_TAX_INFO}) {if $module_data.PRODUCTS_SHIPPING_LINK}{$module_data.PRODUCTS_SHIPPING_LINK}{/if}</span>

            <div class="baseprice-grid">{$module_data.PRODUCTS_BASE_PRICE_FORMATED}</div>
            <div class="actions clearfix"></div>


        </div>

        <div class="add-to-links col-md-12 col-sm-12 col-xs-12">
            <div class="add-to-links-group btn-group">


                <button type="button" class="btn btn-default link-add-basket" data-Qty="1" data-pID="{$module_data.PRODUCTS_ID}" title="{#basket#}" id="add-basket" ><span class="glyphicon glyphicon-arrow-right"></span><span class="glyphicon glyphicon-shopping-cart"></span></button>


                <a href="{$module_data.PRODUCTS_LINK}" title="{$module_data.PRODUCTS_NAME}" class="btn btn-default" title="{#details#}" ><span class="glyphicon glyphicon-search"></span></a>
                {if $smarty.session.customer_id}
                    <button type="button" class="btn btn-default link-wishlist{if $module_data.PRODUCTS_ON_WISHLIST == 'true'} active delete{else} add{/if}" data-pID="{$module_data.PRODUCTS_ID}"  title="{#wishlist#}"><span class="glyphicon glyphicon-star-empty"></span></button>
                {/if}
                <button type="button" class="btn btn-default link-compare{if $module_data.PRODUCTS_COMPARE == 'true'} active delete{else} add{/if}" data-pID="{$module_data.PRODUCTS_ID}"  title="{#compare#}"><span class="glyphicon glyphicon-transfer"></span></button>
            </div>
        </div>
    </div>
</li>


{if preg_match('/RGB/', $module_data.PRODUCTS_FEATURES.2.features.5.feature_value|@strtoupper)}<div class="color red" ></div><div class="color y" ></div><div class="color blue" ></div>
{elseif preg_match('/3-FARBIG/', $module_data.PRODUCTS_FEATURES.2.features.5.feature_value|@strtoupper)}<div class="color red" ></div><div class="color y" ></div><div class="color blue" ></div>
{elseif preg_match('/FARBIG/', $module_data.PRODUCTS_FEATURES.2.features.5.feature_value|@strtoupper)}<div class="color red" ></div><div class="color y" ></div><div class="color blue" ></div>
{elseif preg_match('/CMY/', $module_data.PRODUCTS_FEATURES.2.features.5.feature_value|@strtoupper)}<div class="color c" ></div><div class="color m" ></div><div class="color y" ></div>
{elseif preg_match('/CMYK/', $module_data.PRODUCTS_FEATURES.2.features.5.feature_value|@strtoupper)}<div class="color c" ></div><div class="color m" ></div><div class="color y" ></div><div class="color k" ></div>
{elseif preg_match('/4-FARBIG/', $module_data.PRODUCTS_FEATURES.2.features.5.feature_value|@strtoupper)}<div class="color c" ></div><div class="color m" ></div><div class="color y" ></div><div class="color k" ></div>
{elseif preg_match('/5-FARBIG/', $module_data.PRODUCTS_FEATURES.2.features.5.feature_value|@strtoupper)}<div class="color blue" ></div><div class="color c" ></div><div class="color m" ></div><div class="color rose" ></div><div class="color y" ></div>
{elseif preg_match('/6-FARBIG/', $module_data.PRODUCTS_FEATURES.2.features.5.feature_value|@strtoupper)}<div class="color k" ></div><div class="color blue" ></div><div class="color c" ></div><div class="color m" ></div><div class="color rose" ></div><div class="color y" ></div>
{elseif preg_match('/4 FARBIG/', $module_data.PRODUCTS_FEATURES.2.features.5.feature_value|@strtoupper)}<div class="color c" ></div><div class="color m" ></div><div class="color y" ></div><div class="color k" ></div>
{elseif preg_match('/5 FARBIG/', $module_data.PRODUCTS_FEATURES.2.features.5.feature_value|@strtoupper)}<div class="color blue" ></div><div class="color c" ></div><div class="color m" ></div><div class="color rose" ></div><div class="color y" ></div>
{elseif preg_match('/6 FARBIG/', $module_data.PRODUCTS_FEATURES.2.features.5.feature_value|@strtoupper)}<div class="color k" ></div><div class="color blue" ></div><div class="color c" ></div><div class="color m" ></div><div class="color rose" ></div><div class="color y" ></div>{/if}


{if preg_match('/CYAN/', $module_data.PRODUCTS_FEATURES.2.features.5.feature_value|@strtoupper)}<div class="color c" ></div>{/if}
{if preg_match('/MAGENTA/', $module_data.PRODUCTS_FEATURES.2.features.5.feature_value|@strtoupper)}<div class="color m" ></div>{/if}
{if preg_match('/YELLOW/', $module_data.PRODUCTS_FEATURES.2.features.5.feature_value|@strtoupper)}<div class="color y" ></div>{/if}

{if preg_match('/SCHWARZ/', $module_data.PRODUCTS_FEATURES.2.features.5.feature_value|@strtoupper)}<div class="color k" ></div>{/if}
{if preg_match('/ROT/', $module_data.PRODUCTS_FEATURES.2.features.5.feature_value|@strtoupper)}<div class="color red" ></div>{/if}
{if preg_match('/GELB/', $module_data.PRODUCTS_FEATURES.2.features.5.feature_value|@strtoupper)}<div class="color y" ></div>{/if}
{if preg_match('/HELLBLAU/', $module_data.PRODUCTS_FEATURES.2.features.5.feature_value|@strtoupper)}<div class="color light_blue" ></div>
{elseif preg_match('/BLAU/', $module_data.PRODUCTS_FEATURES.2.features.5.feature_value|@strtoupper)}<div class="color blue" ></div>{/if}
{if preg_match('/GR�N/', $module_data.PRODUCTS_FEATURES.2.features.5.feature_value|@strtoupper)}<div class="color green" ></div>{/if}
{if preg_match('/GRAU/', $module_data.PRODUCTS_FEATURES.2.features.5.feature_value|@strtoupper)}<div class="color grey" ></div>{/if}
{if preg_match('/ORANGE/', $module_data.PRODUCTS_FEATURES.2.features.5.feature_value|@strtoupper)}<div class="color orange" ></div>{/if}
{if preg_match('/ROSA/', $module_data.PRODUCTS_FEATURES.2.features.5.feature_value|@strtoupper)}<div class="color rose" ></div>{/if}
