<div class="print-color-label">
    {if preg_match('/RGB/', $colorValue|@strtoupper)}
            <span class="badge badge-pill" style="background-color: red" title="red">&nbsp;</span>
            <span class="badge badge-pill" style="background-color: yellow" title="yellow">&nbsp;</span>
            <span class="badge badge-pill" style="background-color: blue" title="blue">&nbsp;</span>
    {elseif preg_match('/3-FARBIG/', $colorValue|@strtoupper)}
        <span class="badge badge-pill" style="background-color: red" title="red">&nbsp;</span>
        <span class="badge badge-pill" style="background-color: yellow" title="yellow">&nbsp;</span>
        <span class="badge badge-pill" style="background-color: blue" title="blue">&nbsp;</span>
    {elseif preg_match('/FARBIG/', $colorValue|@strtoupper)}
            <span class="badge badge-pill" style="background-color: red" title="red">&nbsp;</span>
            <span class="badge badge-pill" style="background-color: yellow" title="yellow">&nbsp;</span>
            <span class="badge badge-pill" style="background-color: blue" title="blue">&nbsp;</span>
    {elseif preg_match('/CMY/', $colorValue|@strtoupper)}
        <span class="badge badge-pill" style="background-color: cyan" title="cyan">&nbsp;</span>
        <span class="badge badge-pill" style="background-color: magenta" title="magenta">&nbsp;</span>
        <span class="badge badge-pill" style="background-color: yellow" title="yellow">&nbsp;</span>

    {elseif preg_match('/CMYK/', $colorValue|@strtoupper)}
        <span class="badge badge-pill" style="background-color: cyan" title="cyan">&nbsp;</span>
        <span class="badge badge-pill" style="background-color: magenta" title="magenta">&nbsp;</span>
        <span class="badge badge-pill" style="background-color: yellow" title="yellow">&nbsp;</span>
        <span class="badge badge-pill" style="background-color: black" title="black">&nbsp;</span>

    {elseif preg_match('/4-FARBIG/', $colorValue|@strtoupper)}
            <span class="badge badge-pill" style="background-color: cyan" title="cyan">&nbsp;</span>
            <span class="badge badge-pill" style="background-color: magenta" title="magenta">&nbsp;</span>
            <span class="badge badge-pill" style="background-color: yellow" title="yellow">&nbsp;</span>
            <span class="badge badge-pill" style="background-color: black" title="black">&nbsp;</span>
    {elseif preg_match('/5-FARBIG/', $colorValue|@strtoupper)}<div class="color blue" ></div><div class="color c" ></div><div class="color m" ></div><div class="color rose" ></div><div class="color y" ></div>
    {elseif preg_match('/6-FARBIG/', $colorValue|@strtoupper)}<div class="color k" ></div><div class="color blue" ></div><div class="color c" ></div><div class="color m" ></div><div class="color rose" ></div><div class="color y" ></div>
    {elseif preg_match('/4 FARBIG/', $colorValue|@strtoupper)}<div class="color c" ></div><div class="color m" ></div><div class="color y" ></div><div class="color k" ></div>
    {elseif preg_match('/5 FARBIG/', $colorValue|@strtoupper)}<div class="color blue" ></div><div class="color c" ></div><div class="color m" ></div><div class="color rose" ></div><div class="color y" ></div>
    {elseif preg_match('/6 FARBIG/', $colorValue|@strtoupper)}<div class="color k" ></div><div class="color blue" ></div><div class="color c" ></div><div class="color m" ></div><div class="color rose" ></div><div class="color y" ></div>{/if}


    {if preg_match('/CYAN/', $colorValue|@strtoupper)}<span class="badge badge-pill" style="background-color: cyan" title="cyan">&nbsp;</span>{/if}
    {if preg_match('/MAGENTA/', $colorValue|@strtoupper)}<span class="badge badge-pill" style="background-color: magenta" title="magenta">&nbsp;</span>{/if}
    {if preg_match('/YELLOW/', $colorValue|@strtoupper)}<span class="badge badge-pill" style="background-color: yellow" title="yellow">&nbsp;</span>{/if}

    {if preg_match('/SCHWARZ/', $colorValue|@strtoupper)}<span class="badge badge-pill" style="background-color: black" title="black">&nbsp;</span>{/if}
    {if preg_match('/ROT/', $colorValue|@strtoupper)}<span class="badge badge-pill" style="background-color: red" title="red">&nbsp;</span>{/if}
    {if preg_match('/GELB/', $colorValue|@strtoupper)}<span class="badge badge-pill" style="background-color: yellow" title="yellow">&nbsp;</span>{/if}
    {if preg_match('/HELLBLAU/', $colorValue|@strtoupper)}<span class="badge badge-pill" style="background-color: lightblue" title="light blue">&nbsp;</span>
    {elseif preg_match('/BLAU/', $colorValue|@strtoupper)}<span class="badge badge-pill" style="background-color: blue" title="blue">&nbsp;</span>{/if}
    {if preg_match('/GRÜN/', $colorValue|@strtoupper)}<span class="badge badge-pill" style="background-color: green" title="green">&nbsp;</span>{/if}
    {if preg_match('/GRAU/', $colorValue|@strtoupper)}<span class="badge badge-pill" style="background-color: gray" title="gray">&nbsp;</span>{/if}
    {if preg_match('/ORANGE/', $colorValue|@strtoupper)}<span class="badge badge-pill" style="background-color: orange" title="orange">&nbsp;</span>{/if}
    {if preg_match('/ROSA/', $colorValue|@strtoupper)}<span class="badge badge-pill" style="background-color: mistyrose" title="rose">&nbsp;</span>{/if}
</div>