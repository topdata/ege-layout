
    {if preg_match('/RGB/', $colorValue|@strtoupper)}
        <div class="color-block" style="background-image: linear-gradient(135deg,red,yellow,blue);">&nbsp;</div>
    {elseif preg_match('/3-FARBIG/', $colorValue|@strtoupper)}
        <div class="color-block" style="background-image: linear-gradient(135deg,red,yellow,blue);">&nbsp;</div>
    {elseif preg_match('/FARBIG/', $colorValue|@strtoupper)}
        <div class="color-block" style="background-image: linear-gradient(135deg,red,yellow,blue);">&nbsp;</div>
    {elseif preg_match('/CMY/', $colorValue|@strtoupper)}
        <div class="color-block" style="background-image: linear-gradient(135deg,cyan,magenta,yellow);">&nbsp;</div>

    {elseif preg_match('/CMYK/', $colorValue|@strtoupper)}
        <div class="color-block" style="background-image: linear-gradient(135deg,cyan,magenta,yellow,black);">&nbsp;</div>

    {elseif preg_match('/4-FARBIG/', $colorValue|@strtoupper)}
        <div class="color-block" style="background-image: linear-gradient(135deg,cyan,magenta,yellow,black);">&nbsp;</div>

    {elseif preg_match('/5-FARBIG/', $colorValue|@strtoupper)}<div class="color blue" ></div><div class="color c" ></div><div class="color m" ></div><div class="color rose" ></div><div class="color y" ></div>
    {elseif preg_match('/6-FARBIG/', $colorValue|@strtoupper)}<div class="color k" ></div><div class="color blue" ></div><div class="color c" ></div><div class="color m" ></div><div class="color rose" ></div><div class="color y" ></div>
    {elseif preg_match('/4 FARBIG/', $colorValue|@strtoupper)}<div class="color c" ></div><div class="color m" ></div><div class="color y" ></div><div class="color k" ></div>
    {elseif preg_match('/5 FARBIG/', $colorValue|@strtoupper)}<div class="color blue" ></div><div class="color c" ></div><div class="color m" ></div><div class="color rose" ></div><div class="color y" ></div>
    {elseif preg_match('/6 FARBIG/', $colorValue|@strtoupper)}<div class="color k" ></div><div class="color blue" ></div><div class="color c" ></div><div class="color m" ></div><div class="color rose" ></div><div class="color y" ></div>{/if}


    {if preg_match('/CYAN/', $colorValue|@strtoupper)}<div class="color-block" style="background-color: cyan;">&nbsp;</div> {/if}
    {if preg_match('/MAGENTA/', $colorValue|@strtoupper)}<div class="color-block" style="background-color: magenta" title="magenta">&nbsp;</div>{/if}
    {if preg_match('/YELLOW/', $colorValue|@strtoupper)}<div class="color-block" style="background-color: yellow" title="yellow">&nbsp;</div>{/if}

    {if preg_match('/SCHWARZ/', $colorValue|@strtoupper)}<div class="color-block" style="background-color: black" title="black">&nbsp;</div>{/if}

    {if preg_match('/ROT/', $colorValue|@strtoupper)}<div class="color-block" style="background-color: red" title="red">&nbsp;</div>{/if}
    {if preg_match('/GELB/', $colorValue|@strtoupper)}<div class="color-block" style="background-color: yellow" title="yellow">&nbsp;</div>{/if}
    {if preg_match('/HELLBLAU/', $colorValue|@strtoupper)}<div class="color-block" style="background-color: lightblue" title="light blue">&nbsp;</div>
    {elseif preg_match('/BLAU/', $colorValue|@strtoupper)}<div class="color-block" style="background-color: blue" title="blue">&nbsp;</div>{/if}
    {if preg_match('/GRÜN/', $colorValue|@strtoupper)}<div class="color-block" style="background-color: green" title="green">&nbsp;</div>{/if}
    {if preg_match('/GRAU/', $colorValue|@strtoupper)}<div class="color-block" style="background-color: gray" title="gray">&nbsp;</div>{/if}
    {if preg_match('/ORANGE/', $colorValue|@strtoupper)}<div class="color-block" style="background-color: orange" title="orange">&nbsp;</div>{/if}
    {if preg_match('/ROSA/', $colorValue|@strtoupper)}<div class="color-block" style="background-color: mistyrose" title="rose">&nbsp;</div>{/if}


    {if preg_match('/CYAN,MAGENTA,YELLOW/', $colorValue|@strtoupper)}
    <div class="color-block" style="background-image: linear-gradient(135deg,cyan,magenta,yellow);">&nbsp;</div>
    {elseif preg_match('/GELB,CYAN,MAGENTA/', $colorValue|@strtoupper)}
        <div class="color-block" style="background-image: linear-gradient(135deg,yellow,magenta,cyan);">&nbsp;</div>
    {elseif preg_match('/GELB,CYAN,MAGENTA,BLACK/', $colorValue|@strtoupper)}
        <div class="color-block" style="background-image: linear-gradient(135deg,yellow,magenta,cyan,black);">&nbsp;</div>
    {/if}

