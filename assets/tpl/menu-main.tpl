{get_category max_lvl=1 assign="menu"}
{assign var="count" value=0}
<nav class="navbar navbar-expand-xl px-0">
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <i class="fas fa-bars"></i>
    </button>
    <div class="navbar-collapse collapse" id="navbarSupportedContent">
        <ul class="nav nav-pills nav-fill">
            <!--<li class="nav-item"><a href="#" class="nav-link active">{#text_home#}</a></li>-->
            {foreach name="menu" item="cat" from=$menu}
                <li class="nav-item dropdown">
                    <a href="#" class="nav-link dropdown-toggle" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{$cat.categories_name}</a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        {foreach name="drop_menu" item="parent_cat" from=$cat.childs}
                            <a class="dropdown-item" href="{$parent_cat.url}">{$parent_cat.categories_name}</a>
                        {/foreach}
                    </div>
                </li>
            {/foreach}
            {$box_CONTENT}
            {$box_THEME_STORE}
        </ul>
    </div>
</nav>
