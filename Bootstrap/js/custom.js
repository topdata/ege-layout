

/*=============================================================
 Authour URI: www.binarytheme.com
 License: Commons Attribution 3.0

 http://creativecommons.org/licenses/by/3.0/

 100% To use For Personal And Commercial Use.
 IN EXCHANGE JUST GIVE US CREDITS AND TELL YOUR FRIENDS ABOUT US

 ========================================================  */




(function ($) {
    "use strict";
    var mainApp = {

        main_fun: function () {

            var isMobile = window.matchMedia("only screen and (max-width: 760px)");


            // SHRINK NAVBAR SCRIPT
            $(window).scroll(function() {
                if ($(document).scrollTop() > 70) {
                    //$('.header-top').addClass('shrink');
                    $('.main-menue').addClass('navbar-fixed-top');
                    $(".mini-cart").show();
                    if (!isMobile.matches){
                        $('.navbar-brand').removeClass('hidden');
                    }


                } else {
                    // $('header').removeClass('shrink');
                    $('.main-menue').removeClass('navbar-fixed-top');
                    $(".mini-cart").hide();
                    if (!isMobile.matches) {
                        $('.navbar-brand').addClass('hidden');
                    }
                }
            });

            // SLIDESHOW SCRIPT
            $('.carousel').carousel({
                interval: 3000 //TIME IN MILLI SECONDS
            });


            /*====================================
             WRITE YOUR SCRIPTS BELOW
             ======================================*/
/*

            $(document).bind("ajaxSend", function(){
                $("#loading-indicator").show();
            }).bind("ajaxComplete", function(){
                $("#loading-indicator").hide();
            });
*/


            $("#menu-toggle").click(function(e){e.preventDefault(); $(".sidebar-wrapper").toggleClass("toggled");});


            //pass the images to Fancybox
            /*$("#product-image")
                .bind("click", function(e) {var ez = $('#product-image').data('elevateZoom'); $.fancybox(ez.getGalleryList());return false;})
                .elevateZoom({zoomType : "inner",gallery:'moreimages-slider', cursor: 'crosshair', galleryActiveClass: 'active'});*/


            $(".tab-content.sidebar-content").niceScroll({cursorcolor: "#424242"});
            $("#sidebar-content").niceScroll({cursorcolor: "#424242"});
            $("#product-reviews").niceScroll({cursorcolor: "#424242"});



            $("#popover-graduated-price")
                .popover({placement:'bottom',html: true, title:$("#popover-graduated-price-title").text(), content:$("#popover-graduated-price-content").html()})
                .title = $("#popover-graduated-price-title").text()
            ;

            jQuery(function($){ $('ul#moreimages-slider').easyPaginate({step:6});});

            if (!isMobile.matches) {
                //Conditional script here

                if($("#cross-selling-slider").children().length > 4) {
                    $('#cross-selling-slider').bxSlider({
                        auto: true,
                        pause: 5000,
                        pager: false,
                        minSlides: 1,
                        maxSlides: 6,
                        speed: 5000,
                        slideWidth: 280,
                        slideMargin: 6
                    });
                }

                if($("#also-purchased-slider").children().length > 4) {
                    $('#also-purchased-slider').bxSlider({
                        auto: true,
                        pause: 5000,
                        pager: false,
                        minSlides: 1,
                        maxSlides: 6,
                        speed: 5000,
                        slideWidth: 280,
                        slideMargin: 6
                    });
                }

                if($("#product-info-list-slider").children().length > 4) {
                    $('#product-info-list-slider').bxSlider({
                        auto: true,
                        pause: 5000,
                        pager: false,
                        minSlides: 1,
                        maxSlides: 6,
                        speed: 5000,
                        slideWidth: 280,
                        slideMargin: 6
                    });
                }

                if($("#best-sellers-slider").children().length > 4) {
                    $('#best-sellers-slider').bxSlider({
                        auto: true,
                        pager: false,
                        pause: 5000,
                        minSlides: 1,
                        maxSlides: 6,
                        speed: 5000,
                        slideWidth: 280,
                        slideMargin: 6
                    });
                }

                if($("#new-products-default-slider").children().length > 4) {
                    $('#new-products-default-slider').bxSlider({
                        auto: true,
                        pager: false,
                        pause: 5000,
                        minSlides: 1,
                        maxSlides: 6,
                        speed: 5000,
                        slideWidth: 280,
                        slideMargin: 6
                    });
                }

                if($("#box-specials-slider").children().length > 4) {
                    $('#box-specials-slider').bxSlider({
                        auto: true,
                        pause: 5000,
                        pager: false,
                        minSlides: 1,
                        maxSlides: 6,
                        speed: 5000,
                        slideWidth: 280,
                        slideMargin: 6
                    });
                }

                if($("#box-sp-products-1-slider").children().length > 4){

                    $('#box-sp-products-1-slider').bxSlider({
                        auto: true,
                        pager:false,
                        pause: 5000,
                        minSlides: 1,
                        maxSlides: 6,
                        speed:5000,
                        slideWidth: 280,
                        slideMargin: 6
                    });
                }

                if($("#box-sp-products-2-slider").children().length > 4) {
                    $('#box-sp-products-2-slider').bxSlider({
                        auto: true,
                        pager: false,
                        pause: 5000,
                        minSlides: 1,
                        maxSlides: 6,
                        speed: 5000,
                        slideWidth: 280,
                        slideMargin: 6
                    });
                }

                if($("#box-sp-products-3-slider").children().length > 4) {
                    $('#box-sp-products-3-slider').bxSlider({
                        auto: true,
                        pager: false,
                        pause: 5000,
                        minSlides: 1,
                        maxSlides: 6,
                        speed: 5000,
                        slideWidth: 280,
                        slideMargin: 6
                    });
                }

            }

            //AJAX calls ----------------------------------------  //
/*

            $('#textsearch_input_tmp').typeahead({
                order: "asc",
                minLength: 3,
                groupMaxItem: 15,
                hint: true,
                display: "title",
                cache: true,
                dynamicFilter: [{
                    selector: '#type-select',
                    key: '&type'
                }],
                source: {
                    keywords : {
                        url: ["/ajax.php?method=typeheads","data.keywords"]
                    },
                    device : {
                        url: ["/ajax.php?method=typeheads","data.device"]
                    },
                },
                callback: {
                },
                debug: true
            });
*/



            $(".list-group-selection .list-group-item").click(function(e){

                $(".list-group-selection input[type='radio']").each(function(){
                    var $this = $(this);
                    if ($(this).prop('checked')) {
                        $(this).parent().addClass('active');
                    }else{
                        $(this).parent().removeClass('active');
                    }
                });

            });

            $("#toggle-products-list-short").click(function(e){
                e.preventDefault();
                $(this).toggleClass('active');
                $("#toggle-products-list-short").toggleClass('active');

                $(".products-list").toggleClass("list-wide");
                $(".products-list .item").toggleClass("col-md-3 col-md-12 col-s4 col-sm-12");
                $(".products-list").children().find(".product-image").toggleClass("col-md-2 col-md-12 col-sm-3 col-sm-12");
                $(".products-list").children().find(".price-review").toggleClass("col-md-9  col-md-12 col-sm-7 col-sm-12");
                $(".products-list").children().find('.add-to-links').toggleClass("col-md-1 col-md-12 col-sm-2 col-sm-12");
                $(".products-list").children().find('.add-to-links-group').toggleClass("btn-group btn-group-vertical");
                if($(".products-list").hasClass("list-wide")){
                    $.ajax({url: '/ajax.php',data: {method:'productListing',  view: 'list-wide'}});
                }else{
                    $.ajax({url: '/ajax.php',data: {method:'productListing',  view: 'list-short'}});
                }
            });

            $("#toggle-products-list-wide").click(function(e){
                e.preventDefault();
                $(this).toggleClass('active');
                $("#toggle-products-list-short").toggleClass('active');

                $(".products-list").toggleClass("list-wide");
                $(".products-list .item").toggleClass("col-md-3 col-md-12 col-s4 col-sm-12");
                $(".products-list").children().find(".product-image").toggleClass("col-md-2 col-md-12 col-sm-3 col-sm-12");
                $(".products-list").children().find(".price-review").toggleClass("col-md-9  col-md-12 col-sm-7 col-sm-12");
                $(".products-list").children().find('.add-to-links').toggleClass("col-md-1 col-md-12 col-sm-2 col-sm-12");
                $(".products-list").children().find('.add-to-links-group').toggleClass("btn-group btn-group-vertical");
                if($(".products-list").hasClass("list-wide")){
                    $.ajax({url: '/ajax.php',data: {method:'productListing',  view: 'list-wide'}});
                }else{
                    $.ajax({url: '/ajax.php',data: {method:'productListing',  view: 'list-short'}});
                }
            });


            $('body')
                .on('change',".toggle-checkout-payment input",function(){
                    $("input[name='payment']").each(function(){
                        var $this = $(this);
                        if ($(this).prop('checked')) {
                            $(this).parent().addClass('panel-primary');
                            //$(this).parent().toggleClass('panel-default panel-primary');
                        }else{
                            //if($(this).parent().hasClass('panel-primar')){
                            $(this).parent().removeClass('panel-primary');
                            //}
                        }
                    });


                    //$(this).parent().toggleClass('panel-default panel-primary');
                })
                .on('click', '.link-compare.add', function(e){
                    e.preventDefault();
                    var elem = $(this);
                    $.ajax({
                        url: '/ajax_compare.php',
                        data: { action: 'add_compare', pID: $(this).data('pid'),qty: $(this).data('Qty')},
                        success: function(response){
                            $('.compare_count').text(response);

                            $(elem).removeClass('add').addClass('delete');
                            if($(elem).data('deletetxt')) $(elem).html('<i class="fas fa-exchange-alt"></i> ' + $(elem).data('deletetxt'));


                            if($('.compare_count').parent().hasClass('empty') && response > 0){
                                $('.compare_count').parent().addClass('filled').removeClass('empty');
                                $('.compare_count').parent().fadeOut().fadeIn().fadeOut().fadeIn().fadeOut().fadeIn();
                            }
                        }
                    });
                })
                .on('click', '.link-compare.delete', function(e){
                    e.preventDefault();
                    var elem = $(this);
                    $.ajax({
                        url: '/ajax_compare.php',
                        data: { action: 'delete_compare', pID: $(this).data('pid') },
                        success: function(response){
                            $('.compare_count').text(response);
                            $(elem).removeClass('delete').addClass('add');
                            if($(elem).data('addtxt')) $(elem).html('<i class="fas fa-exchange-alt"></i> ' + $(elem).data('addtxt'));

                            if(response == 0){
                                $('.compare_count').parent().addClass('empty').removeClass('filled');
                            }
                        }
                    });
                })
                .on('click', '.link-wishlist.add', function(e){
                    e.preventDefault();
                    var elem = $(this);
                    $.ajax({
                        url: '/ajax.php',
                        data: {method:'wishlist',  action: 'add', pID: $(this).data('pid') },
                        success: function(response){
                            $('.wishlist_count').text(response);

                            $(elem).removeClass('add').addClass('delete');
                            if($(elem).data('deletetxt')) $(elem).html('<i class="fas fa-star"></i> ' + $(elem).data('deletetxt'));


                            if($('.wishlist_count').parent().hasClass('empty') && response > 0){
                                $('.wishlist_count').parent().addClass('filled').removeClass('empty');
                                $('.wishlist_count').parent().fadeOut(slow).fadeIn(fast).fadeOut(slow).fadeIn(fast).fadeOut().fadeIn().fadeOut().fadeIn().fadeOut().fadeIn();
                            }
                        }
                    });
                })
                .on('click', '.link-wishlist.delete', function(e){
                    e.preventDefault();
                    var elem = $(this);
                    $.ajax({
                        url: '/ajax.php',
                        data: {method:'wishlist', action: 'delete', pID: $(this).data('pid') },
                        success: function(response){
                            $('.wishlist_count').text(response);
                            $(elem).removeClass('delete').addClass('add');
                            if($(elem).data('addtxt')) $(elem).html('<i class="far fa-star"></i> ' + $(elem).data('addtxt'));

                            if(response == 0){
                                $('.compare_count').parent().addClass('empty').removeClass('filled');
                            }
                        }
                    });
                })
                .on('click', '.link-spylist.delete', function(e){
                    e.preventDefault();
                    //var $elem = $(this);
                    var li = $(this).parent().parent().parent().parent();
                    $.ajax({
                        url: '/ajax.php',
                        data: {method:'spylist', action: 'delete', SpyID: $(this).data('spyid') },
                        success: function(response){
                            if(response.stat == true){
                                li.fadeOut(400, function(){
                                    li.remove();
                                });
                            }
                        }
                    });
                })
                .on('click', '.link-add-basket', function(e){
                    e.preventDefault();
                    $.ajax({
                        url: '/ajax.php',
                        data: {method:'shopping-cart', action: 'add', pID: $(this).data('pid') },
                        dataType: 'json',

                        success: function(response){
                            $('.shopping-cart-quantity').text(response.count);
                            $('.shopping-cart-price').text(response.total);

                        }
                    });

                })
                .on('submit', '#quick_add', function(e){
                    e.preventDefault();
                    $.ajax({
                        url: '/ajax.php',
                        type: 'POST',
                        data: {method:'quickie', action: 'add', pModel: $('#quick_add #quickie').val() },
                        dataType: 'json',

                        success: function(response){
                            if(response.status == true){
                                $('.shopping-cart-quantity').text(response.count);
                                $('.shopping-cart-price').text(response.total);
                                $('.add-quickie-response').html('<div class="alert alert-success" roll="alert">'+response.message+'</div>');
                            }else{
                                $('.add-quickie-response').html('<div class="alert alert-danger" roll="alert">'+response.message+'</div>');
                            }
                        }
                    });


                })
                .on('click', '.btn-cart', function(e){
                    e.preventDefault();
                    var $promoPrice = false;
                    if($('#products-promo-price').val()){
                        $promoPrice = $('#products-promo-price').val();
                        $.ajax({
                            url: '/ajax.php',
                            data: {method:'shopping-cart', action: 'add', pID: $(this).data('pid'),'Qty': $('#product-info-qty').val(),promo_price : $promoPrice },
                            dataType: 'json',

                            success: function(response){
                                $('.shopping-cart-quantity').text(response.count);
                                $('.shopping-cart-price').text(response.total);

                            }
                        });
                    }else{
                        $.ajax({
                            url: '/ajax.php',
                            data: {method:'shopping-cart', action: 'add', pID: $(this).data('pid'),'Qty': $('#product-info-qty').val()},
                            dataType: 'json',

                            success: function(response){
                                $('.shopping-cart-quantity').text(response.count);
                                $('.shopping-cart-price').text(response.total);

                            }
                        });
                    }
                })
                .on('click', '.link-delete-basket', function(e){
                    e.preventDefault();
                    var td = $(this).parent();
                    var tr = td.parent();

                    $.ajax({
                        url: '/ajax.php',
                        data: {method:'shopping-cart', action: 'delete', CartID: $(this).data('cartid') },
                        dataType: 'json',

                        success: function(response){
                            $('.shopping-cart-quantity').text(response.count);
                            $('.shopping-cart-price').text(response.total);

                            tr.fadeOut(400, function(){
                                tr.remove();
                            });
                        }
                    });

                })
                .on('click', '.link-update-shopping-cart', function(e){
                    e.preventDefault();

                    var data = {method:'shopping-cart', action: 'update', products: $("#shopping-cart-update").serializeArray()};

                    console.log(data);

                    $.ajax({
                        url: '/ajax.php',
                        data: data,
                        dataType: 'json',

                        success: function(response){
                            console.log(response);
                            location.reload();
                        }
                    });


                })
                .on('click', '.link-delete-item', function(e){
                    var $form=$(this).closest('form');
                    e.preventDefault();
                    var $elem = $(this);
                    var td = $(this).parent().parent();
                    var tr = td.parent();

                    $('#myModalConfimation').modal({show:true})
                        .one('click', '#delete', function() {

                            $('#myModalConfimation').modal('hide');
                            $.ajax({
                                url: '/ajax.php',
                                data: {method:'CategoriesFavored', action: 'delete', CategoriesFavoredID: $elem.data('catid') },
                                dataType: 'json',

                                success: function(response){
                                    if(response.stat == true){
                                        tr.fadeOut(400, function(){
                                            tr.remove();
                                        });
                                    }
                                }
                            });


                            $form.trigger('submit'); // submit the form
                        });
                    // .one() is NOT a typo of .on()
                })
                .on('click', '.link-compare-ajax', function(e){
                    e.preventDefault();
                    var $elem = $(this);

                    $('#modalDialogFullWidth').find('.modal-body').load('/ajax_compare.php?action=show_compare');
                    $('#modalDialogFullWidth').modal({show:true});

                })
                .on('change', 'select.typeahead-select', function(e){
                    var $form=$(this.form);
                    if($('#type-select').val() == 'device'){
                        $form.attr('action','/advanced_cat_search_result.php');
                    }else{
                        $form.attr('action','/advanced_search_result.php');
                    }


                })
                .on('click', '.link-box-printers', function(e){
                    e.preventDefault();
                    var $elem = $(this);
                    $.ajax({
                        url: '/ajax.php',
                        data: { method: 'Printers', lvl: 'manufacturers' },
                        success: function(response){
                            if(response.status == true) {


                                $('#modalDialog').find('.modal-title').text($elem.data('title'));
                                $('#modalDialog').find('.modal-body').html(response.dropDownManufacturers+'<div style="margin-top:10px;" class="box-printers-series"></div>');
                                $('#modalDialog').modal({show: true});
                            }
                        }
                    });

                })
                .on('change', '.printer-select-manufacturers', function(e){
                    e.preventDefault();
                    var $elem = $(this);
                    $('#modalDialog').find('.box-printers-series').html('');
                    $elem.attr('disabled', true);
                    $.ajax({
                        url: '/ajax.php',
                        data: { method: 'Printers', lvl: 'series', mID: $elem.val()},
                        success: function(response){
                            if(response.status == true) {
                                $('#modalDialog').find('.box-printers-series').html(response.dropDownSeries+'<div style="margin-top:10px;" class="box-printers-printers" data-mid="'+$elem.val()+'"></div>');
                            }
                            $elem.attr('disabled', false);
                        }
                    });

                })
                .on('change', '.printer-select-series', function(e){
                    e.preventDefault();
                    var $elem = $(this);
                    $('#modalDialog').find('.box-printers-printers').html('');
                    $('#modalDialog').find('.printer-select-manufacturers').attr('disabled', true);
                    $elem.attr('disabled', true);
                    $.ajax({
                        url: '/ajax.php',
                        data: { method: 'Printers', lvl: 'printer', mID: $('#modalDialog').find('.box-printers-printers').data('mid') ,sID: $elem.val()},
                        success: function(response){
                            if(response.status == true) {
                                $('#modalDialog').find('.box-printers-printers').html(response.dropDownPrinters+'<div style="margin-top:10px;" class="box-printers-printers-info"></div>');
                            }
                            $('#modalDialog').find('.printer-select-manufacturers').attr('disabled', false);
                            $elem.attr('disabled', false);
                        }
                    });

                })
                .on('change', '.printer-select-printers', function(e){
                    e.preventDefault();
                    var $elem = $(this);
                    var $buttonAdd = '<a class="btn btn-primary" href="/categories_favored_edit.php?action=new&printers_id='+$elem.val()+'">'+$('#text_to_categories_favored').data('title')+'</a>&nbsp;';
                    var $buttonShow = '<a class="btn btn-success" href="/index.php?printer='+$elem.val()+'">'+$('#text_search_show').data('title')+'</a>';
                    $('#modalDialog').find('.box-printers-printers-info').html($buttonAdd+$buttonShow)


                })
            ;


            $('#password').pwstrength(
                {
                    ui: {
                        showVerdicts: false,
                    }
                }
            );
            $(function () {$('[data-toggle="tooltip"]').tooltip()})
        },


        initialization: function () {
            mainApp.main_fun();

        }

    }
    // Initializing ///

    $(document).ready(function () {
        mainApp.main_fun();
    });

}(jQuery));

