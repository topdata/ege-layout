<div id="{block name="modal-id"}myModal{/block}" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                {block name="modal-header"}
                <h4 class="modal-title">{block name="modal-title"}Confirmation{/block}</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                {/block}
            </div>
            <div class="modal-body">
                {block name="modal-body"}
                    <p>Do you want to save changes you made to document before closing?</p>
                    <p class="text-warning"><small>If you don't save, your changes will be lost.</small></p>
                {/block}
            </div>
            <div class="modal-footer">
                {block name="modal-footer"}
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button>
                {/block}
            </div>
        </div>
    </div>
</div>