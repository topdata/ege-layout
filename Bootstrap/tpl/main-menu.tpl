<div class="main-menu">
	<div class="row">
		<ul class="navi">
			{get_category max_lvl=1 assign="menu"}
			{assign var="count" value=0}
			{foreach name="menu" item="cat" from=$menu}
				{*assign var="count" value=$count+1*}
				<li{if $cat.class || isset($cat.childs)} class="{$cat.class}{if isset($cat.childs)} parent-item{/if}"{/if}><a href="{$cat.url}" title="{$cat.categories_name}" class="menu-item" target="{$cat.target}">
				{$cat.categories_name}
         	<div class="overState{if !isset($cat.childs)} overStateNone{/if}">{$cat.categories_name}</div>
				</a>          
				{if isset($cat.childs)}
					<ul class="subnav">
					{foreach name="submenu" item="child" from=$cat.childs}
						<li class="child {if $child.class || isset($child.childs)} {$child.class}{if isset($child.childs)} parent-item{/if}{/if}"><a href="{$child.url}" title="{$child.categories_name|@strip_tags}" class="menu-item" target="{$child.target}">{$child.categories_name}</a></li>
					{/foreach}
					</ul>
				{/if}
				</li>
			{/foreach}
	  </ul>
	  <div class="nav-childs" style="display: none;"></div>
	</div>
</div>