(function ($) {
    "use strict";
    var InkAndToner = {

        render: function () {
            this.mId = 0;


            $.ajax({
                url: '/ajax.php',
                data: {method: 'Printers', lvl: 'manufacturers'},
                success: function (response) {
                    if (response.status == true) {
                        $('#ink_and_toner_finder').find('.brand').html(response.dropDownManufacturers);

                        $(".printer-select-manufacturers").bind("change", function (e) {
                            e.preventDefault();
                            var $elem = $(this);
                            $('#ink_and_toner_finder').find('.model-series').html('');
                            $elem.attr('disabled', true);

                            InkAndToner.setManufacturerId($elem.val());

                            $.ajax({
                                url: '/ajax.php',
                                data: {method: 'Printers', lvl: 'series', mID: $elem.val()},
                                success: function (response) {
                                    if (response.status == true) {
                                        $('#ink_and_toner_finder').find('.model-series').html(response.dropDownSeries);
                                    }
                                    $elem.attr('disabled', false);


                                    $(".printer-select-series").bind("change", function (e) {
                                        e.preventDefault();
                                        var $elem = $(this);
                                        $('#ink_and_toner_finder').find('.printer-model').html('');
                                        $('#ink_and_toner_finder').find('.printer-select-manufacturers').attr('disabled', true);
                                        $elem.attr('disabled', true);

                                        $.ajax({
                                            url: '/ajax.php',
                                            data: {
                                                method: 'Printers',
                                                lvl: 'printer',
                                                mID: InkAndToner.getManufacturerId(),
                                                sID: $elem.val()
                                            },
                                            success: function (response) {
                                                if (response.status == true) {
                                                    $('#ink_and_toner_finder').find('.printer-model').html(response.dropDownPrinters);




                                                    $(".printer-select-printers").bind("change", function (e) {
                                                        e.preventDefault();
                                                        var $elem = $(this)
                                                        //alert('jiha');
                                                        window.location.replace('/index.php?printer='+$elem.val());
                                                    })
                                                    ;


                                                }
                                                $('#ink_and_toner_finder').find('.printer-select-manufacturers').attr('disabled', false);
                                                $elem.attr('disabled', false);
                                            }
                                        });

                                    });
                                }
                            });

                        });


                    }
                }
            });



        },

        setManufacturerId: function (manufacturerId) {
          this.mId= manufacturerId;
        },

        getManufacturerId: function () {
            return this.mId;
        },

        initialization: function () {
            InkAndToner.render();

        }

    }
    // Initializing ///
    $(document).ready(function () {
        InkAndToner.render();
    });

}(jQuery));