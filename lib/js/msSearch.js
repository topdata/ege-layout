
$.widget( "custom.catcomplete", $.ui.autocomplete, {

  _create: function() {
    this._super();
    this.widget().menu( "option", "items", "> :not(.ui-autocomplete-category)" );
  },

  _resizeMenu: function() {
    this.menu.element.outerWidth(470);
  },

  _renderMenu: function( ul, items ) {
    var that = this,
      currentCategory = "";
    $.each( items, function( index, item ) {
      var li;
      if ( item.itemType != currentCategory ) {
        var categoryLabel = "";
        if(item.itemType == 'product')
        {
          categoryLabel = "Artikel";
        } else if(item.itemType == 'category'){
          categoryLabel = "Kategorie";
        } else {
          categoryLabel = "Drucker";
        }
        ul.append( "<li class='ui-autocomplete-category " + item.itemType + " text-primary font-weight-bold'>" + categoryLabel + "</li>" );
        currentCategory = item.itemType;
      }
      li = that._renderItemData( ul, item );
      if ( item.category ) {
        li.attr( "aria-label", item.itemType + " : " + item.label );
      }
    });
  },

  _renderItem: function( ul, item ) {
    var image = '<img class="rounded float-left mr-3" style="height:30px;" src="'+item.image+'">';
    var label = '<span class="text">' + item.label + '</span>';
    return $( "<li>" )
      .addClass(item.itemType)
      .attr( "data-value", item.value )
      .append(image).append(label)
      .appendTo( ul );
  }
});


$(function() {
  $( ".keyword-search" ).catcomplete({
    delay: 3,
    minLength: 2,
    select: function( event, ui ) {
      //console.log(ui.item);
      if(ui.item.itemType == 'printer')
      {
        window.location.href = '/'+ui.item.label+'_d'+ui.item.id+'.html';
      } else if(ui.item.itemType == 'category'){
        window.location.href = '/'+ui.item.label+'_c'+ui.item.id+'.html';
      } else {
        window.location.href = '/'+ui.item.label+'_p'+ui.item.id+'.html';
      }
    },
    source: function(request, response) {
      var postData = { "type": 'combined', "q": encodeURI(request.term) };
      $.ajax({
        url: "/esearch.php",
        type: "POST",
        dataType: "JSON",
        data: postData,
        success: function(data) {
          response($.map(data.hits.hits, function(item) {
            return {
              label: item._source.label,
              category: item._source.brand,
              id: item._source.id,
              itemType: item._type,
              image: item._source.image
            }
          }));
        },
      });
    }
  });
}

);