$(document).ready(function () {
    init();
});

function init() {
    //scale_height('.products-grid .product-image');
    //scale_height('.products-grid .price-review');
    //scale_height('.products-grid .add-to-links');
}

function scale_height(objects, repeat)
{
    if (typeof repeat == 'undefined')
        repeat = false;
    h = 0;
    $(objects).each(function (k, v) {
        //$(v).height('auto');
        if (h < $(v).height())
            h = $(v).height();
    });
    if (h > 0) {
        $(objects).height(h);
        //$(objects).css('min-height',h+'px'); ///doof wegen padding

        if (repeat != false) {
            if (repeat != h || h == 0) {
                setTimeout(function () {
                    console.log(h);
                    $(objects).css('height', 'auto');
                    scale_height(objects, h);
                }, 200);
            }
        }
    }
}

